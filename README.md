# Spring Boot Mustache Example

## Things to do list:
1. Clone this repository: `git clone https://gitlab.com/hendisantika/spring-boot-mustache-example.git`.
2. Go to the folder: `cd spring-boot-mustache-example`.
3. Run the application: `mvn clean spring-boot:run`.
4. Open your favorite browser: http://localhost:8080

## Image Screen shot
Home Page

![Home Page](img/index.png "Home Page")

List User Page

![List User Page](img/list.png "List User Page")

![List User Page](img/list2.png "List User Page")