package com.hendisantika.springbootmustacheexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootMustacheExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootMustacheExampleApplication.class, args);
	}

	//Override MustacheAutoConfiguration to support defaultValue("")
	/*@Bean
	public Mustache.Compiler mustacheCompiler(Mustache.TemplateLoader mustacheTemplateLoader,
											  Environment environment) {

		MustacheEnvironmentCollector collector = new MustacheEnvironmentCollector();
		collector.setEnvironment(environment);

		// default value
		Mustache.Compiler compiler = Mustache.compiler().defaultValue("")
				.withLoader(mustacheTemplateLoader)
				.withCollector(collector);
		return compiler;
	}*/
}
