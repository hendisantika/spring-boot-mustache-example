package com.hendisantika.springbootmustacheexample.repository;

import com.hendisantika.springbootmustacheexample.domain.User;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-mustache-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 21/01/20
 * Time: 07.11
 */
public interface UserRepository extends PagingAndSortingRepository<User, Long>,

        JpaSpecificationExecutor<User> {

}