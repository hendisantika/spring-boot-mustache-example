CREATE TABLE `users` (

  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,

  `name` VARCHAR(255) NOT NULL,

  `email` VARCHAR(255) NOT NULL,

  `phone` VARCHAR(255),

  `address` VARCHAR(255),

  PRIMARY KEY (`id`)

) ENGINE=MYISAM DEFAULT CHARSET=latin1;



CREATE UNIQUE INDEX id_user_email ON users (email(255));